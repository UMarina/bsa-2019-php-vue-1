Vue.component('modal', {
    template: '#modal-template'
})

var app = new Vue ({
    el: '#app',
    data: {
        loading: true,
        loadError: false,
        showAddModal: false,
        showEditModal: false,
        showDeleteModal: false,
        searchType: 'name',
        keyword: '',
        currentUser: [],
        newUser: [],
        users: []
    },
    mounted: function () {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                this.users = response.data;
                this.users.forEach((user) => {
                    user.avatar = 'src/images/a_' + user.id + '.jpg';
                })
            })
            .catch((error) => {this.loadError = true})
            .finally(() => {this.loading = false});
    },
    computed: {
        filterUsers: function(){
            let keyword = this.keyword;
            let type = this.searchType;
            if(keyword === '') return this.users;
            return this.users.filter(function (elem) {
                if (type === "email") {
                    return elem.email.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                } else if (type === "name") {
                    return elem.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                }
            })
        }
    },
    methods: {
        addUser: function() {
            if (this.newUser.name && this.newUser.email) {
                let maxId = Math.max(...this.users.map(user => user.id), 0);
                this.newUser.id = maxId + 1;
                this.newUser.avatar = 'src/images/a_' + this.newUser.id + '.jpg';
                this.users.push(this.newUser);
                this.newUser = [];
            }
            this.showAddModal = false
        },
        editUser: function (user) {
            this.currentUser = {
                id: user.id,
                name: user.name,
                email: user.email,
                website: user.website,
                avatar: user.avatar,
            };
            this.currentUser.index = this.users.indexOf(user);
            this.showEditModal = true;
        },
        openConfirmDelete: function (user) {
            this.currentUser = user;
            this.showDeleteModal = true;
        },
        updateUser: function (index) {
            this.users[index].name = this.currentUser.name;
            this.users[index].email = this.currentUser.email;
            this.users[index].website = this.currentUser.website;
            this.users[index].phone = this.currentUser.phone;
            this.currentUser = [];
            this.showEditModal = false
        },
        deleteUser: function (user) {
            this.users.splice(this.users.indexOf(user), 1);
            this.currentUser = [];
            this.showDeleteModal = false;
        }
    }
});


